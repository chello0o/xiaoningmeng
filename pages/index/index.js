// index.js
// 获取应用实例
const app = getApp()
var green = "../../images/green.png";
var yellow = "../../images/yellow.png";
var icon = "../../images/icon.png";
var shareTitle = '江宁区新冠病毒疫苗接种点';
let plugin = requirePlugin('routePlan');
let key = '';  //使用在腾讯位置服务申请的key
let referer = '小宁萌';   //调用插件的app的名称
var itemList = ["小程序地图","地图APP"];
var mks = [
   {
    id: 1,
    latitude: 31.932018,
    longitude: 118.894457,
    width: 50,
    height: 50,
    iconPath: green,
    title: "南京医科大学附属逸夫医院"
  }
];

Page({
  data: {
    Height: 0,
    scale: 15,
    latitude: "",
    longitude: "",
    markers: []
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function(options) {
    let vm = this;
    console.log("options:"+options);
    if(options){
      vm.getLocation();
    }
    if (wx.getUserProfile) {
      vm.setData({
        canIUseGetUserProfile: true
      });
      wx.getSystemInfo({
        success: function (res) {
          //设置map高度，根据当前设备宽高满屏显示
          vm.setData({
            view: {
              Height: res.windowHeight
            }
          })
        }
      });
      vm.getUserLocation();
    }
  },
  getUserLocation: function () {
    let vm = this;
    wx.getSetting({
      success: (res) => {
        console.log("getSetting");
        console.log(res)
        // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
        // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
        // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      vm.getLocation();
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          vm.getLocation();
        } else {
          //调用wx.getLocation的API
          vm.getLocation();
        }
      }
    })
  },
  // 微信获得经纬度
  getLocation: function () {
    let _this = this;
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        _this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: mks
        })
      },
      fail: function (res) {
        console.log('fail' + JSON.stringify(res))
      }
    })
  },
  //点击merkers
  markertap(e){
    console.log(e);
    console.log(e.markerId);
    console.log(e.detail.markerId);
    console.log(this.data.scale);
    wx.showActionSheet({
      itemList: itemList,
      success: function (res) {
        console.log(res.tapIndex);
        mks.forEach(function(item, index){
          if(e.markerId == item.id){
            console.log(item); //这里的item就是从数组里拿出来的每一个每一组
            if(0 === res.tapIndex){
                let endPoint = JSON.stringify({  //终点
                  'name': item.title,
                  'latitude': item.latitude,
                  'longitude': item.longitude
                });
                wx.navigateTo({
                  url: 'plugin://routePlan/index?key=' + key + '&referer=' + referer + '&endPoint=' + endPoint
                });
            } else {
              wx.openLocation({
                latitude: item.latitude,
                longitude: item.longitude,
                name: item.title,
                scale: 15
              })
            }
          }
        })
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },
  
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  //用户点击右上角分享给朋友
  onShareAppMessage: function () {
    return {
      title: shareTitle,
      desc: '分享吧',
      path: '/pages/index/index'
    }
  },
  //用户点击右上角分享朋友圈
	onShareTimeline: function () {
		return {
	      title: shareTitle,
	      query: {
	        latitude: 31.953588,
          longitude: 118.839783
	      },
	      imageUrl: icon
	    }
	}
})
